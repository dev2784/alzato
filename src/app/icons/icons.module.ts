import { NgModule } from '@angular/core';

import { FeatherModule } from 'angular-feather';
import { Phone, CheckSquare, Facebook, Instagram, Youtube } from 'angular-feather/icons';

const icons = {
  Phone,
  CheckSquare,
  Facebook,
  Instagram,
  Youtube
}
@NgModule({
  declarations: [],
  imports: [
    FeatherModule.pick(icons)
  ],
  exports:[
    FeatherModule
  ]
})
export class IconsModule { }
